# Ansible

> Ansible playbooks, vaults, inevtory and much more.

## Initial Setup

1. **Configure Inventory file.**

   Default location of inventory file is `/etc/ansible/hosts`
   Edit that file and add you server groups with their ip addresses. Your sample inventory file look like as below;

   ```bash
   # List of server IP's
   [testHostGroup]
   172.0.0.1
   172.0.0.2

   # Global ansible variables for perticular server group.
   [testHostGroup:vars]
   ansible_user=<username>
   ansible_password=<password>
   ansible_become_pass=<sudo_password>
   ```

2. **Setup Password-less authentication (optional)**

   For password less authentication you can exchange ssh keys. perform following steps;

   ```bash
   # On control node

   # 1. Generate ssh key.
   ssh-keygen

   # 2. Copy public ssh key to host machine.
   cat .ssh/id_rsa.pub | ssh <username>@<IP> 'cat >> .ssh/authorized_keys'

   # 3. Change permissions.
   ssh <username>@<IP> "chmod 700 .ssh; chmod 640 .ssh/authorized_keys"
   ```

   After exchanging ssh keys you will be able to communicate with your host machine without any password.

3. **Ping all hosts.**

   Hola! you are almost there. To check whether you are all set just ping all hosts with following command.

   ```bash
   ansible -m ping all

   # you should get this response.
    172.17.0.2 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
    }
   ```

4. **Runing your first playbook.**

   To Run your first playbook run following command.

   ```bash
   ansible-playbook first-playbook.yml

   # If you want to run playbook as sudo.
   ansible-playbook apche-install.yml --ask-become-pass
   ```
